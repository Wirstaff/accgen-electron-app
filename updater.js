const autoUpdater = require("electron-updater").autoUpdater;
const { dialog, app } = require("electron");
const fs = require("fs");
const path = require("path");
const fetch = require("node-fetch");

var dataPath = undefined;
var ipcevent = undefined;

function sendStatusToWindow(message) {
    if (message)
        console.log(message);
    if (ipcevent)
        ipcevent.reply('accgen-web-alert-msg', {
            status: message,
            electron: true
        });
}

exports.initialized = false;

async function initializeUpdate() {
    try {
        var response = await fetch.default("https://cathook.club/s/accgen/electronupdater");
        autoUpdater.setFeedURL({
            provider: "generic",
            url: response.url
        });
    } catch (error) {
        console.log("Failed to get dynamic updater URL!", error);
        autoUpdater.setFeedURL({
            provider: "generic",
            url: "https://nullworks.gitlab.io/accgen/accgen-electron-app/"
        });
    }
    await autoUpdater.checkForUpdates();
}

// Not async on purpose to prevent a race condition
exports.default = function (event) {
    if (event)
        ipcevent = event;
    if (exports.initialized)
        return;

    dataPath = app.getPath('userData');
    console.log(dataPath);

    exports.initialized = true;

    initializeUpdate();
}

///////////////////
// Auto updater  //
///////////////////
autoUpdater.autoDownload = true;

autoUpdater.on('update-available', function (info) {
    sendStatusToWindow('Update available. Downloading!');
    if (!ipcevent)
        dialog.showMessageBox(null, {
            type: 'warning',
            buttons: ['Ok'],
            defaultId: 2,
            title: 'Updater running in the background',
            message: 'The page is not loading correctly. An update is being downloaded in the background which may fix this issue.'
        })
});

autoUpdater.on('error', function (err) {
    console.error(err)
    sendStatusToWindow('Auto updater error! Manually download here: https://gitlab.com/nullworks/accgen/accgen-electron-app/-/jobs/artifacts/master/browse/dist?job=build');
});

autoUpdater.on('download-progress', function (progressObj) {
    let log_message = 'Downloading update - Downloaded ' + parseInt(progressObj.percent) + '%';
    sendStatusToWindow(log_message);
});

function updateNotifications(info) {
    if (dataPath) {
        if (fs.existsSync(path.join(dataPath, "latest_update.txt"))) {
            var ver = fs.readFileSync(path.join(dataPath, "latest_update.txt"))
            if (ver == info.version) {
                sendStatusToWindow("Last update failed! Manually download here: https://gitlab.com/nullworks/accgen/accgen-electron-app/-/jobs/artifacts/master/browse/dist?job=build")
                return;
            } else
                fs.writeFileSync(path.join(dataPath, "latest_update.txt"), info.version)
        } else
            fs.writeFileSync(path.join(dataPath, "latest_update.txt"), info.version)
    }
    sendStatusToWindow("Update downloaded! Will be installed next time you restart the app!");
    setTimeout(() => {
        sendStatusToWindow(undefined);
    }, 30000);
}

autoUpdater.on('update-downloaded', function (info) {
    //sendStatusToWindow('Update downloaded; will install in 1 seconds');
    if (ipcevent)
        updateNotifications(info);
    else
        autoUpdater.quitAndInstall(false, true);
});